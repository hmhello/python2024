def prime(n):
    if n == 1: return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


# 把n分解成两个素数p,q，满足 p+q=n
#O(n)
n = 1000
for p in range(2, n):
    q = n - p
    if prime(p) and prime(q) and p + q == n:
        print(p, q)
