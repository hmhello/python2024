def prime(n):
    if n == 1: return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

# 把n分解成两个素数p,q，满足 p+q=n
#枚举 复杂度O(n^2) 算法复杂度O(n^3)
n=100
for p in range(2,n):
    for q in range (2,n):
        if prime(p) and prime(q) and p+q==n:
            print(p,q)