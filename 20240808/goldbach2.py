def prime(n):
    if n == 1: return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


# 把n分解成两个素数p,q，满足 p+q=n 约定p<=q
n = 1000000
for p in range(3, n, 2):
    q = n - p
    if p>q :break
    if prime(p) and prime(q) and p + q == n:
        print(p, q)

