def prime(n):
    if n == 1: return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


# 验证哥德巴赫猜想，对于大于6的每一个偶数n都可以分解成两个素数p,q，满足 p+q=n 约定p<=q
N = 200000
for n in range(6, N + 1, 2):
    for p in range(3, n, 2):
        q = n - p
        if p > q: break
        if prime(p) and prime(q) and p + q == n:
            print(f'{n}={p}+{q}')
            break
