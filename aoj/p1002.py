import math

a, b, c = map(int, input().strip().split())
# print(a, b, c)
x1 = 0
x2 = 0

d = b * b - 4 * a * c
x1 = (-b + math.sqrt(d)) / (2 * a)
x2 = (-b - math.sqrt(d)) / (2 * a)

if x1 >= x2:
    print('%.2f' % x1, '%.2f' % x2)
else:
    print('%.2f' % x2, '%.2f' % x1)
