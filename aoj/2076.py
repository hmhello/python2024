s = 46979
hours = s // 3600
minutes = (s % 3600) // 60
seconds = (s % 3600) % 60
print(f'{hours}:{minutes}:{seconds}')


a, b, c = map(int, input().split())

if a < b and b < c:
    print("Yes")
else:
    print("No")