# 交换法 冒泡排序  每一轮通过相邻的交换把最大值沉于数组末尾

a, b, c, d = map(int, input().strip().split())

if a > b:
    t = a
    a = b
    b = t

if b > c:
    t = b
    b = c
    c = t
if c > d:
    t = c
    c = d
    d = t

if a > b:
    t = a
    a = b
    b = t

if b > c:
    t = b
    b = c
    c = t

if a > b:
    t = a
    a = b
    b = t

print(a, b, c, d)
