def is_circle_inside_rectangle(W, H, x, y, r):
    if x - r >= 0 and y - r >= 0 and x + r <= W and y + r <= H:
        return "Yes"
    else:
        return "No"
input= list(map(int, input().split()))
W, H, x, y, r = input
print(is_circle_inside_rectangle(W, H, x, y, r))