def bubble_sort(arr):
    n = len(arr)
    for i in range(n - 1, 0, -1):
        for j in range(0, i):
            if arr[j] > arr[j + 1]:
                t = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = t


arr = [1,2, 3, 4,9 ,8 ,7 ,6,-10, 10 ,-5 ,2]
bubble_sort(arr)
for x in arr:
    print(x, end=" ")