# 数学最大最小法 缺点是缺乏可扩展性无法用于更多的数据排序
while True:
    a, b, c = map(int, input().strip().split())
    min, mid, max = 0, 0, 0
    max = a
    if b > max: max = b
    if c > max: max = c
    min = a
    if b < min:
        min = b
    if c < min:
        min = c
    mid = a + b + c - min - max
    print(min, mid, max)
