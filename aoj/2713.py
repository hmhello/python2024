while True:
    try:
        x, y = map(int, input().strip().split())
        if x == 0 and y == 0:
            break
        if x>y:
            t = x
            x = y
            y = t
        print(x, y)
    except:
        break

'''

3 2
2 2
5 3
0 0
样例输出
2 3
2 2
3 5
'''
