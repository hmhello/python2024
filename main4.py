def g(a, b, r):
    s = 0
    for i in range(a, b + 1):
        if i % r == 0:
            s = s + i
    return s

def h(a, b, r):
    s = 0
    while a % r != 0:
        a = a + 1
    while b % r != 0:
        b = b - 1
    s = (a + b) * ((b - a) / r + 1) // 2
    return (s)

def f(a, b, r):
    s = 0
    a = (a + r - 1) // r * r
    b = b // r * r
    s = (a + b) * ((b - a) // r + 1) // 2
    return (s)

a = 1
b = 10000000000
s35 = f(a, b, 3) + f(a, b, 5) - f(a, b, 15)
print(s35)
