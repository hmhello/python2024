while True:
    try:
        s = input().strip()
        s = '00' + s
        n = len(s)
        w = n // 3
        right = s[n - w::]
        mid = s[n - 2 * w:n - w:]
        left = s[n - 3 * w:n - 2 * w:]
        print(int(left) + int(right) - int(mid))
    except:

        break
