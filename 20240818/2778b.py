import sys

# sys.stdin.readline()
for line in sys.stdin.readlines():
    if not line.strip():
        continue
    s = '00' + line.strip()
    n = len(s)
    w = n // 3
    right = s[n - w::]
    mid = s[n - 2 * w:n - w:]
    left = s[n - 3 * w:n - 2 * w:]
    print(int(left) + int(right) - int(mid))
