import sys

in_file = open("A.in")
# in_file = sys.stdin

out_file = open("A.out", "w")
# out_file = sys.stdout


for line in in_file.readlines():
    a, b = map(int, line.strip().split())
    out_file.write("%d\n" % (a + b))

in_file.close()
out_file.close()
