import sys

in_file = open("2725.in")
# in_file = sys.stdin

# out_file = open("2725.out", "w")
# out_file = sys.stdout
B = [
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ],
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ],
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ],
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
]
n = int(in_file.readline())
for line in in_file.readlines():
    b, f, r, v = map(int, line.strip().split())
    B[b - 1][f - 1][r - 1] += v

for i in range(4):
    if i > 0:
        print("#" * 20)
    for j in range(3):
        for k in range(10):
            print(B[i][j][k], end=" ")
        print()

in_file.close()
# out_file.close()
