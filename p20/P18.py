def ones(n):
    '''
    函数作用是计算n中有多少个1
    :param n: 被检测的整数
    :return n: 中多少个1
    '''
    s = str(n)
    return s.count('1')


assert (ones(1) == 1)
assert (ones(112) == 2)
numbers = list(range(1, 101))
print(numbers)
print(list(map(ones, numbers)))

tot = 0
m = 204
for i in range(1, 100000):
    tot += ones(i)
    if tot >= m:
        if tot == m:
            print(i)
        else:
            print("Error")
        break
