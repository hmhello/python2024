if __name__ == '__main__':
    import tool

    for k in range(0, 31):
        a, b, c, d, e = k / 16, (k % 16) / 8, (k % 8) / 4, (k % 4) / 2, (k % 2)
        if (a + b + c + d + e == 3 and a + c <= 2 and b + c > 0 and tool.imply(c == 1, d + e == 1) and tool.imply(
                    b == 1, d + e < 2)):
            print(a, b, c, d, e)
