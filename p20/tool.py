import math


def solve(total, money, fx, fy, fz):
    for x in range(total + 1):
        for y in range(total - x + 1):
            z = total - x - y
            if math.fabs(fx * x + fy * y + z * fz - money) <= 1E-15:
                print(x, y, z)


def prime(n):
    for c in range(2, 1 + int(math.sqrt(n))):
        if n % c == 0:
            return False
    return True


def imply(p, q):
    return not p or p and q
