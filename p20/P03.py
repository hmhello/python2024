'''
3.	打印下列图形：
            1
           121
          12321
           121
            1
'''


def f(n):
    for i in range(n):
        print(' ' * (n - 1 - i), end='')  # print  C-i spaces
        # print('1' * (1 + 2 * i))  # print  C'+2i numbers
        for j in range(1, i + 2):
            print(j, end='')
        for j in range(i, 0, -1):
            print(j, end='')
        print()

    for i in range(n-2,-1,-1):
        print(' ' * (n - 1 - i), end='')  # print  C-i spaces
        # print('1' * (1 + 2 * i))  # print  C'+2i numbers
        for j in range(1, i + 2):
            print(j, end='')
        for j in range(i, 0, -1):
            print(j, end='')
        print()


f(9)
