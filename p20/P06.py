# 6.赵、钱、孙、李、周五人围着一张圆桌吃饭。饭后，周回忆说：“吃饭时，赵坐在钱旁边，钱的左边是孙或李”；
# 李回忆说：“钱坐在孙左边，我挨着孙坐”。结果他们一句也没有说对。请问，他们在怎样坐的？

def left(p, q):
    return (p, q) in [(0, 1), (1, 2), (2, 3), (3, 4), (4, 0)]


def right(p, q):
    return left(q, p)


def near(p, q):
    return left(p, q) or right(p, q)


zhao = 0
for qian in [1, 2, 3, 4]:
    for sun in [1, 2, 3, 4]:
        for li in [1, 2, 3, 4]:
            for zhou in [1, 2, 3, 4]:
                if qian == sun or qian == li or qian == zhou: continue
                if sun == li or sun == zhou: continue
                if li == zhou: continue
                zhou_first = near(zhao, qian)  # 赵坐在钱旁边，
                zhou_second = left(sun, qian) or left(li, qian)  # 钱的左边是孙或李”；
                li_first = left(qian, sun)  # 钱坐在孙左边，
                li_second = near(li, sun)  # 我挨着孙坐”。
                if zhou_first or zhou_second or li_second or li_first: continue
                print(zhao, qian, sun, li, zhou)
