'''
10.	A、B、C三人进入决赛，赛前A说：“B和C得第二，我得第一”；B说：“我进入前两名，C得第三名”；
C说：“A不是第二，B不是第一”。比赛产生了一、二、三名，比赛结果显示：获得第一的选手全说对了，
获得第二的选手说对了一句，获得第三的选手全说错了。编程求出A、B、C三名选手的名次。

'''
for a in [1, 2, 3]:
    for b in [1, 2, 3]:
        for c in [1, 2, 3]:
            if a == b or a == c: continue
            if b == c: continue
            a1 = (b == 2 and c == 2)
            a2 = (a == 1)
            b1 = (b <= 2)
            b2 = (c == 3)
            c1 = (a != 2)
            c2 = (b != 1)

            if a + a1 + a2 != 3: continue
            if b + b1 + b2 != 3: continue
            if c + c1 + c2 != 3: continue

            print(a, b, c)
