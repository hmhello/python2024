'''
11.	甲、乙、丙、丁四人共有糖若干块，甲先拿出一些糖分给另外三人，使他们三人的糖数加倍；
乙拿出一些糖分给另外三人，也使他们三人的糖数加倍；丙、丁也照此办理
，此时甲、乙、丙、丁四人各有16块，编程求出四个人开始各有糖多少块。

'''

n = 64

for a in range(n//2, n):
    for b in range(0, n):
        if a + b > n: continue
        for c in range(0, n):
            if a + b + c > n: continue
            d = n - a - b - c
            aa, bb, cc, dd = a, b, c, d
            # 甲先拿出一些糖分给另外三人，使他们三人的糖数加倍
            aa -= bb + cc + dd
            bb *= 2
            cc *= 2
            dd *= 2
            # 乙先拿出一些糖分给另外三人，使他们三人的糖数加倍
            bb -= aa + cc + dd
            aa *= 2
            cc *= 2
            dd *= 2
            # 丙先拿出一些糖分给另外三人，使他们三人的糖数加倍
            cc -= aa + bb + dd
            aa *= 2
            bb *= 2
            dd *= 2
            # 丁先拿出一些糖分给另外三人，使他们三人的糖数加倍
            dd -= aa + bb + cc
            aa *= 2
            bb *= 2
            cc *= 2
            if aa == bb and bb == cc and cc == dd:
                print(a, b, c, d)
                break
