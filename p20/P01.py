# 1.	随机产生一些1—100之间的整数，直到产生的数为50为止。
import random


def doss(a, b):  # randint()
    return int(random.random() * (b - a + 1)) + a


while True:
    k = doss(1, 100)
    print(k, end=' ')
    if k == 50:
        break
