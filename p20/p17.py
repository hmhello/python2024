import sys


def p17(n=6):
    for x in range(n, 1000000, 10):
        a = x // 10
        b = int("6" + str(a))
        if b == x * 4:
            print("%d * 4= %d" % (x, b))
            break


p17()
