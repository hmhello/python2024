'''
3.	打印下列图形：
            1
           121
          12321
           121
            1
'''


def abs(x):
    if x > 0:
        return x
    else:
        return -x


def f(n):
    m = 2 * n + 1
    for x in range(0, m):
        for y in range(0, m):
            if abs(x - n) + abs(y - n) <= n:
                print(n - abs(x - n) - abs(y - n) + 1, end='')
            else:
                print(' ', end='')
        print()


f(6)
