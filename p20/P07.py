if __name__ == '__main__':
    import sys, tool

    for i in range(100, 1000):
        head, mid, tail = i / 100, (i % 100) / 10, i % 10
        if head != mid and mid != tail and tail != head and mid > head + tail and not tool.prime(mid + head):
            sys.stdout.write(str(head * 100 + mid * 10 + tail) + " ")
    print ('')
    
