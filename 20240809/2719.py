while True:
    W, H = map(int, input().strip().split())
    if W == 0 and H == 0:
        break
    for i in range(W):
        for j in range(H):
            print('#', end='')
        print()
    print()