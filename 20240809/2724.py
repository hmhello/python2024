S = [0] * 14
H = [0] * 14
C = [0] * 14
D = [0] * 14
cards = [S, H, C, D]

n = int(input())
for i in range(n):
    kind, rank = input().strip().split()
    rank = int(rank)

    if kind == 'S':
        S[rank] = 1
    if kind == 'H':
        H[rank] = 1
    if kind == 'C':
        C[rank] = 1
    if kind == 'D':
        D[rank] = 1

for i in range(1, 14):
    if S[i] == 0:
        print('S', i)
for i in range(1, 14):
    if H[i] == 0:
        print('H', i)
for i in range(1, 14):
    if C[i] == 0:
        print('C', i)
for i in range(1, 14):
    if D[i] == 0:
        print('D', i)
