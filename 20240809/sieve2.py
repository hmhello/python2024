# 输出1-100的所有素数

# 素数判断模块

N = 100000
is_prime = [0] * (N + 1)


def sieve():
    for i in range(0, N + 1):  is_prime[i] = True
    is_prime[0] = is_prime[1] = False
    for p in range(2, N):
        if p * p > N:
            break
        if is_prime[p]:
            for i in range(p * 2, N + 1, p):
                is_prime[i] = False


sieve()
for i in range(0, N + 1):
    if is_prime[i]:
        print(i, end=' ')
