while True:
    W, H = map(int, input().strip().split())
    if W == 0 and H == 0:
        break
    for i in range(W):
        for j in range(H):
            char = ""
            if (i+j)%2==0:
                char = '#'
            else:
                char = '.'

            print(char, end='')
        print()
    print()