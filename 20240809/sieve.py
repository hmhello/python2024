# 输出1-100的所有素数

# 素数判断模块

N = 100
is_prime = [0] * (N + 1)

for i in range(0, N + 1):
    is_prime[i] = True

is_prime[0] = is_prime[1] = False
for p in [2,3,5,7]:
    for i in range(p * 2, N + 1, p):
        is_prime[i] = False

for i in range(0, N + 1):
    if is_prime[i]:
        print(i, end=' ')
