import sys

dic = {}
for line in sys.stdin.readlines()[1:]:
    a, b = map(int, line.strip().split())
    k = a / b
    if k in dic.keys():
        dic[k] += 1
    else:
        dic[k] = 1
print(max(dic.values()))
