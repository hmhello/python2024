balloons = ["yellow", "red", "yellow", "white"]

d = {}  # 创建一个空的字典 d=dict()

for balloon in balloons:
    if balloon in d:
        d[balloon] += 1
    else:
        d[balloon] = 1

print(d)
