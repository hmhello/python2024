words =input().strip().split()

d = {}  # 创建一个空的字典 d=dict()

for word in words:
    if word in d:
        d[word] += 1
    else:
        d[word] = 1

print(len(words))
for key in sorted(d.keys()):
    print(key+":"+str(d[key]))

