import random


def coin():
    if random.random() < 0.5:
        return 1
    else:
        return 0


n = 10
for i in range(n):
    print(coin(), end=' ')