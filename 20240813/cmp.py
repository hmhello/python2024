arr = [1, 3, 5, 7, 9, 8, 2, 4, 6, 8, 0, 13]
arr.sort(key=lambda x: -x)  # 降序排序


# arr.sort(key=lambda x:x)#升序排序（默认）

def f(x):
    if x < 10:
        return x
    else:
        return x % 10 + f(x // 10)


arr.sort(key=f)  #
print(arr)
print(sorted(arr))
