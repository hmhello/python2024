double = lambda x: 2 * x
square = lambda x: x * x


def cube(x):
    return x * x * x


for i in range(10):
    print(i, double(i), square(i), (lambda x:x**3)(i))
