import random

n = 10000
m = 0

for i in range(n):
    x = random.random()
    y = random.random()
    if x * x + y * y <= 1:
        m += 1
pi = 4 * m / n
print(pi)
