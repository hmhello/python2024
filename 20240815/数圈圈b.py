import sys


def count(char):
    if char in '069':
        return 1
    if '8' == char:
        return 2
    return 0


for line in sys.stdin.readlines():
    print(sum(map(count, line.strip())))
