def gcd(a, b):
    while True:
        if b == 0:
            break
        r = a % b
        a = b
        b = r
    return a


assert (gcd(6, 8) == 2)
