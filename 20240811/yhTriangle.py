def fact(n):
    # TODO
    ans = 1
    for i in range(2, n + 1):
        ans *= i
    return ans


assert (fact(5) == 120)  # 单元测试 actualValue==expectedValue


def cnr(n, r):
    return fact(n) // fact(n - r) // fact(r)


assert (cnr(5, 3) == 10)  # 单元测试


def main():
    n = 5
    for i in range(0, n):
        for j in range(0, i + 1):
            print(cnr(i, j), end=" ")
        print()


main()
