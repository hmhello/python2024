def bubble_sort(arr, left, right):
    if left >= right: return
    for j in range(left, right):
        if arr[j] > arr[j + 1]:
            t = arr[j]
            arr[j] = arr[j + 1]
            arr[j + 1] = t
    bubble_sort(arr, left, right - 1)


arr = [1, 2, 3, 4, 9, 8, 7, 6, -10, 10, -5, 2]
bubble_sort(arr, 0, len(arr) - 1)
for x in arr:
    print(x, end=" ")
