def binomial(n, r):
    # TODO
    if r == 0 or r == n:
        return 1
    else:
        return binomial(n - 1, r - 1) + binomial(n - 1, r)


assert (binomial(5, 0) == 1)
assert (binomial(5, 5) == 1)
assert (binomial(5, 3) == 10)
binomial(50,25)