def f(n):
    if n < 10:
        print(n)
    else:
        print(n % 10)
        f(n // 10)


n = 1234567
f(n)
