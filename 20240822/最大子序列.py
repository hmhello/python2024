a = [-7, 1, 2, -1, 3, -2]
n = len(a)

'''立方算法O(n^3)'''

best = 0
bx = 0
by = 0
for x in range(0, n):
    for y in range(x, n):
        s = 0
        for k in range(x, y + 1):
            s += a[k]
        if s > best:
            best = s
            bx = x
            by = y

print(best,bx,by)
