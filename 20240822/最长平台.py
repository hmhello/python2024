'''
f = [0] * 51
f[1] = f[2] = 1
print(f)
for i in range(3, 50 + 1):
    f[i] = f[i - 1] + f[i - 2]
'''

a = [1, 2, 2, 3, 3, 3, 4, 5, 5, 6]


dp = [0] * len(a)
print(dp)
dp[0] = 1

for i in range(1, len(a)):
    if a[i] == a[i - 1]:
        dp[i] = dp[i - 1] + 1
    else:
        dp[i] = 1
print(max(dp))
