'''
离线计算  打表法
O(1)
'''

f = [0] * 51
f[1] = f[2] = 1
print(f)
for i in range(3, 50 + 1):
    f[i] = f[i - 1] + f[i - 2]

for i in range(3, 50 + 1):
    print(f[i])
