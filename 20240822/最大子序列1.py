a = [-7, 1, 2, -1, 3, -2]
n = len(a)

'''平方算法'''
dp = [0] * len(a)  # dp部分和  dp[i]=a[0]+....+a[i] =>dp[i]=dp[i-1]+a[i]
dp[0] = a[0]
for i in range(1, n):
    dp[i] = dp[i - 1] + a[i]

print(dp)

best = 0
bx = 0
by = 0
for x in range(0, n):
    for y in range(x, n):
        s = 0
        if x == 0:
            s = dp[y]
        else:
            s = dp[y] - dp[x - 1]
        if s > best:
            best = s
            bx = x
            by = y

print(best, bx, by)
