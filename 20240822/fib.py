'''
函数计算 无记忆性 每次都要重复计算
O(n)=1.618^n
'''


def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


for i in range(1, 50 + 1):
    print(fib(i))
