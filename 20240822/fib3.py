'''
记忆化搜索 学习算法 递归的优化技术
1. 先查表（记忆)
2. 如果不存在就计算 ，并及时把答案存入表中
'''

f = [0] * 51


def fib(n):
    if f[n] != 0:
        return f[n]
    if n <= 2:
        f[n] = 1
        return f[n]
    else:
        f[n] = fib(n - 1) + fib(n - 2)
        return f[n]


print('before call',f)
fib(50)
print('after call fib(3)',f)
