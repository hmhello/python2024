import math


# 素数判断模块 改进版2 找到一个非平凡因子（[2,sqrt(n)]之间的因子）就退出
def prime(n):
    if n == 1: return False
    for i in range(2, n):
        if i * i > n: break
        if n % i == 0:
            return False
    return True


assert(prime(1)==False)

