import math


# 素数判断模块 改进版3 找到一个非平凡因子（[3,sqrt(n)]之间的奇数因子 提高效率
def prime(n):
    if n == 1: return False
    if n % 2 == 0:
        if n == 2:
            return True
        else:
            return False
    else:
        for i in range(3, n, 2):
            if i * i > n: break
            if n % i == 0:
                return False
        return True


flag = prime(1000000007)
print(flag)
