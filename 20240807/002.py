# 素数判断模块
def prime1(n):
    cnt = 0
    for i in range(1, n + 1):
        if n % i == 0:
            cnt += 1

    if cnt == 2:
        return True
    else:
        return False


# 素数判断模块 改进版 找到一个非平凡因子（[2,n-1]之间的因子）就退出
def prime2(n):
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


# 素数判断模块 改进版2 找到一个非平凡因子（[2,sqrt(n)]之间的因子）就退出
def prime3(n):
    for i in range(2, n):
        if i * i > n: break
        if n % i == 0:
            return False
    return True


# 素数判断模块 改进版3 找到一个非平凡因子（[3,sqrt(n)]之间的奇数因子）
def prime4(n):
    if n % 2 == 0:
        if n == 2:
            return True
        else:
            return False
    else:
        for i in range(3, n, 2):
            if i * i > n: break
            if n % i == 0:
                return False
        return True


# 输出1-n中素数个数
n = 1e5

cnt = 0
for i in range(2, int(n + 1)):
    if prime4(i):
        cnt += 1
print(cnt)
