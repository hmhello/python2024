# 素数判断模块 改进版 找到一个非平凡因子（[2,n-1]之间的因子）就退出
def prime(n):
    if n == 1: return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


flag = prime(1000000007)
print(flag)
