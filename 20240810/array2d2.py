def create2d(r, c):
    a = []
    for i in range(r):
        a.append([0] * c)
    return a


a = create2d(3, 4)
print(a)
r = len(a)  # a的行数
c = len(a[0])  # a的列数
print(r, c)
tot = 0

for i in range(r):
    for j in range(c):
        tot += a[i][j]

print(tot)
