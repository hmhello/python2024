def create2d(r, c):
    a = []
    for i in range(r):
        a.append([0] * c)
    return a


A = create2d(3, 4)
for i in range(3):
    a, b, c, d = map(int, input().strip().split())
    A[i][0] = a
    A[i][1] = b
    A[i][2] = c
    A[i][3] = d
# print(A)
mr, mc = 0, 0  #最大值所在的坐标

for i in range(3):
    for j in range(4):
        if A[i][j] > A[mr][mc]:
            mr = i
            mc = j

print(A[mr][mc])
print(mc + 1)
print(mr + 1)
