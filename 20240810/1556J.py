def create2d(r, c):
    a = []
    for i in range(r):
        a.append([0] * c)
    return a


def show2d(a):
    for i in range(len(a)):
        for j in range(i+1):
            print(a[i][j], end=' ')
        print()


n = int(input())
A = create2d(n, n)
# A[i][j]=C(i,j)

for i in range(n):   A[i][0] = 1  # 第一列置为1 A[i][0]=1

for i in range(n):   A[i][i] = 1  # 主对角线置为1 A[i][i]=1

for i in range(2,n):
    for j in range(1,i):
        A[i][j]=A[i-1][j-1]+A[i-1][j]

show2d(A)
