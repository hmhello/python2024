a = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

r = len(a)  # a的行数
c = len(a[0])  # a的列数
print(r,c)
tot = 0

for i in range(r):
    for j in range(c):
        tot += a[i][j]

print(tot)
