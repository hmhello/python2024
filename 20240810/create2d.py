def create2d(r, c):
    a = []
    for i in range(r):
        a.append([0] * c)
    return a


a = create2d(3, 4)
print(a)
