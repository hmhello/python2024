def create2d(r, c):
    a = []
    for i in range(r):
        a.append([0] * c)
    return a


def show2d(a):
    for i in range(len(a)):
        for j in range(len(a[i])):
            print(a[i][j], end=' ')
        print()


def solve(n):
    a = create2d(n, n)
    x, y = n - 1, n // 2
    a[x][y] = 1

    for k in range(2, n * n + 1):
        nx = (x + 1) % n
        ny = (y + 1) % n
        if a[nx][ny] == 0:
            a[nx][ny] = k
        else:
            nx = (x - 1 + n) % n
            ny = y
            a[nx][ny] = k
        x = nx
        y = ny
    show2d(a)


def main():
    while True:
        try:
            n = int(input())
            solve(n)
            print()
        except:
            break


main()
